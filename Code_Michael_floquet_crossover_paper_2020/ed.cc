// code to solve small electron-phonon problem with two electrons on two sites

// new version for Floquet-Dicke: with local spin-spin correlation spectrum <Sz(t) Sz(t')> exp(i w (t-t'))

// new version with Peierls
// new version with classical Peierls (added May 6, 2019)

// new version with initial state = product of coherent state (ALPHA) and electronic groundstate

#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
#include <vector>
#include <assert.h>
#include <math.h>
#include "matrix.h"
#ifndef NO_MPI
    #include <mpi.h>
#endif


using namespace std;

typedef complex<double> cdouble;
typedef matrix<cdouble> cmat;
cdouble II(0,1);

#define NSITES      2
#define NHILBPHON   30
#define NHILBREDUCED   60 // 2*NHILBPHON
#define NHILB       120 // 4*NHILBPHON
//#define SECONDORDER // if defined, use old version with A^2 instead of full Peierls
#define ASQUARED    1.0 // prefactor of A^2 terms to switch it on or off
#define OMEGA       6.0 
#define OMEGAPUMP   6.0
#define JHOP        1.00
#define G1          0.20 // coupling to bond current
#define U1          8.00 // Hubbard U
#define U0          8.00 // different from U1: quench U0 -> U1
#define FMAX        0.00
#define AMAX        0.00    // classical Peierls field
#define CWDRIVE     // if defined use cw drive; else use pulse
#define FWIDTH      3.0
#define FCENTER     15.0
#define TMAX        200.
#define NUMT        800

#define ALPHA       3. //alpha of coherent initial state

// for spectrum
#define TARPES      100.0
#define SIGARPES    36.0  // sigma for ARPES = probe duration
#define NTARPES     72  // 2*NTARPES+1 = number of time points for ARPES around TARPES = 6 sigma window
#define NOMEGAARPES 600
#define OMEGAMIN    0.20
#define OMEGAMAX    0.80

// LAPACK stuff

extern "C" void zgetrf_(int*, int*, cdouble*, int*, int*, int*);
extern "C" void zgetri_(int*, cdouble*, int*, int*, cdouble*, int*, int*);
void invert(cdouble* G, int msize)
{
    int LWORK = msize, INFO=0;
    cdouble work[NHILB]; // use maximal size here
    assert(NHILB>=msize);
    int permutations[msize];
    
    zgetrf_(&msize, &msize, G, &msize, permutations, &INFO);
    if(INFO != 0)
    {
        cout << "Complex matrix inverse: Error at zgetrf. INFO: " << INFO<< endl;
    }
    
    zgetri_(&msize, G, &msize, permutations, work, &LWORK, &INFO);
    if(INFO != 0)
    {
        cout << "Complex matrix inverse: Error at zgetri. INFO: " << INFO<< endl;
    }
    
}

extern "C" void zheev_(char* jobz, char* uplo, int* N, cdouble* H, int* LDA, double* W, cdouble* work, int* lwork, double* rwork, int *info);

void diagonalize(cmat &H, vector<double> &evals, int N)
{
    int     matsize = N;
    int     lwork = 2*(2*N-1);
    double  rwork[3*N-2];
    cdouble work[2*(2*NHILB-1)]; // use maximal size here
    char    jobz = 'V';
    char    uplo = 'U';
    int     info;
    
	zheev_(&jobz, &uplo, &matsize, H.ptr(), &matsize, &evals[0], &work[0], &lwork, &rwork[0], &info);
	assert(!info);
}

void times(cmat &A, cmat &B, cmat &C, int N)
{
    for(int i=0; i<N; ++i)
        for(int j=0; j<N; ++j)
        {
            C(i,j) = 0.;
            for(int k=0; k<N; ++k)
            {
                C(i,j) += A(i,k)*B(k,j);
            }
        }
}

void set_U_step(cmat &U, cmat &A1, cmat &A2, vector<double> &evals, int N, double dtstep)
{
    cmat tmp1(N, N);
    cmat tmp2(N, N);
    
    cmat B1(N, N);
    cmat B2(N, N);
    
    double c1 = (3.-2.*sqrt(3.))/12.;
    double c2 = (3.+2.*sqrt(3.))/12.;
    
    for(int i=0; i < N; i++)
        for(int j=0; j < N; j++)
        {
            B1(i,j) = c1*A1(i,j) + c2*A2(i,j);
            B2(i,j) = c2*A1(i,j) + c1*A2(i,j);
        }
    
    
    diagonalize(B1, evals, N);
    for(int i=0; i < N; i++)
    {
        for(int j=0; j < N; j++)
        {
            tmp1(i,j) = 0.;
            for(int k=0; k < N; k++)
            {
                tmp1(i,j) += B1(i,k) * exp(-II * dtstep * evals[k]) * conj(B1(j,k)) ;
            }
        }
    }
    
    diagonalize(B2, evals, N);
    for(int i=0; i < N; i++)
    {
        for(int j=0; j < N; j++)
        {
            tmp2(i,j) = 0.;
            for(int k=0; k < N; k++)
            {
                tmp2(i,j) += B2(i,k) * exp(-II * dtstep * evals[k]) * conj(B2(j,k)) ;
            }
        }
    }
    
    times(tmp1, tmp2, U, N);
}

inline double shape(double time)
{
    return exp(-(time-FCENTER)*(time-FCENTER)/(2.*FWIDTH*FWIDTH));
}

inline int factorial(int n)
{
    int result = 1;
    for(int j=1; j<=n; ++j)
        result *= j;
        
    return result;
}

inline double arpesshape(double time1, double time2, double time0, double sigma)
{
    return exp(-(time1-time0)*(time1-time0)/(2.*sigma*sigma)) * exp(-(time2-time0)*(time2-time0)/(2.*sigma*sigma)) / (sigma*2.*M_PI); // only one sigma in denominator for norm
}

inline double faculty(int m)
{
    double dummy = 1;
    for(int n=1; n<=m; ++n)
        dummy *= double(n);
    return dummy;
}

int main(int argc, char * argv[])
{
    //************** MPI INIT ***************************
  	int numprocs=1, myrank=0, namelen;
    
#ifndef NO_MPI
  	char processor_name[MPI_MAX_PROCESSOR_NAME];
  	MPI_Init(&argc, &argv);
  	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  	MPI_Get_processor_name(processor_name, &namelen);
    
	MPI_Barrier(MPI_COMM_WORLD);
    
	MPI_Datatype ttype  = MPI_DOUBLE;
    
#endif
	if(myrank==0) cout << "\n\tProgram running on " << numprocs << " processors." << endl;
    
#ifndef NO_MPI
	MPI_Barrier(MPI_COMM_WORLD);
#endif
    
    //************** Build the Hilbert space ***************************
    
    // 2 electrons with spin on 2 sites; I can add Hubbard U if I want
    
    // label all possible states in occupation number representation
    vector<double> nph0(NHILB);
    vector<double> nup0(NHILB);
    vector<double> ndn0(NHILB);
    vector<double> nup1(NHILB);
    vector<double> ndn1(NHILB);
    vector<double> nel0(NHILB);
    vector<double> nel1(NHILB);
    
    // fermionic ordering to clarify signs: site 0 left, site 1 right; up left, down right
    // electrons |up down ; 0 0>
    for(int n0=0; n0<NHILBPHON; ++n0)
        {
            nph0[n0] = n0;
            nup0[n0] = 1;
            ndn0[n0] = 1;
            nup1[n0] = 0;
            ndn1[n0] = 0;
            nel0[n0] = 2;
            nel1[n0] = 0;
        }
    
    // electrons |up 0 ; 0 down>
    for(int n0=0; n0<NHILBPHON; ++n0)
        {
            nph0[NHILBPHON + n0] = n0;
            nup0[NHILBPHON + n0] = 1;
            ndn0[NHILBPHON + n0] = 0;
            nup1[NHILBPHON + n0] = 0;
            ndn1[NHILBPHON + n0] = 1;
            nel0[NHILBPHON + n0] = 1;
            nel1[NHILBPHON + n0] = 1;
        }
    
    // electrons |0 down ; up 0>
    for(int n0=0; n0<NHILBPHON; ++n0)
        {
            nph0[2*NHILBPHON + n0] = n0;
            nup0[2*NHILBPHON + n0] = 0;
            ndn0[2*NHILBPHON + n0] = 1;
            nup1[2*NHILBPHON + n0] = 1;
            ndn1[2*NHILBPHON + n0] = 0;
            nel0[2*NHILBPHON + n0] = 1;
            nel1[2*NHILBPHON + n0] = 1;
        }
    
    // electrons |0 0 ; up down>
    for(int n0=0; n0<NHILBPHON; ++n0)
        {
            nph0[3*NHILBPHON + n0] = n0;
            nup0[3*NHILBPHON + n0] = 0;
            ndn0[3*NHILBPHON + n0] = 0;
            nup1[3*NHILBPHON + n0] = 1;
            ndn1[3*NHILBPHON + n0] = 1;
            nel0[3*NHILBPHON + n0] = 0;
            nel1[3*NHILBPHON + n0] = 2;
        }
    
    
    // construct the exp(i*G1*(a+adag)) operator in bosonic Hilbert space
    // use as basis the ig(a+adag) operator
    // use larger Hilbert space for the Taylor series before projecting
    int NHILBLARGE = 3*NHILBPHON;
    cmat igee(NHILBLARGE,NHILBLARGE);
    for(int n=0; n<NHILBLARGE; ++n)
        for(int m=0; m<NHILBLARGE; ++m)
        {
            igee(n,m) = 0.;
            if(n==m-1)
                igee(n,m) = II*G1*sqrt(m);
            if(n==m+1)
                igee(n,m) = II*G1*sqrt(m+1.);
        }
    
    cmat expigee(NHILBLARGE,NHILBLARGE);
    cmat ordernew(NHILBLARGE,NHILBLARGE);
    cmat orderold(NHILBLARGE,NHILBLARGE);
    for(int n=0; n<NHILBLARGE; ++n)
        for(int m=0; m<NHILBLARGE; ++m)
        {
            expigee(n,m) = 0.;
            orderold(n,m) = 0.;
            if(n==m)
            {
                expigee(n,m) = 1.;
                orderold(n,m) = 1.;
            }
        }
    
    // orderold: (ig)^n/n!
    // ordernew: (ig)^(n+1)/(n+1)!
    
    for(int n=1; n<=NHILBLARGE; ++n) // break after second order to check consistency; should be at least NHILBLARGE
    {
        times(igee, orderold, ordernew, NHILBLARGE);
        ordernew *= double(1./n);
        expigee += ordernew;
        orderold = ordernew;
    }
    
    /*
    for(int n=0; n<NHILBPHON; ++n)
        for(int m=0; m<NHILBPHON; ++m)
        {
            cout << n << '\t' << m << '\t' << expigee(n,m) << endl;
        }
    */
     
     
    // hopping from left to right: 10, 20, 31, 32
    // hopping from right to left: 01, 02, 13, 23
    
    // build static Hamiltonian
    cmat Ham0(NHILB, NHILB);
    cmat Hamkin(NHILB, NHILB);
    cmat Hamu(NHILB, NHILB);
    cmat Hamdocc(NHILB, NHILB);
    cmat Hamu0(NHILB, NHILB); // initial U0 for U quench
    cmat Sz0(NHILB, NHILB); // for spin correlations
    cmat Hamph(NHILB, NHILB);
    cmat Hamx(NHILB, NHILB); // X term, to be multiplied by f(t)
    cmat Hamg1(NHILB, NHILB); // linear interaction
    
    cmat Hamhop(NHILB, NHILB); // for classical Peierls phase
    cmat Hamhopconj(NHILB, NHILB);
    
    for(int i=0; i<NHILB; ++i)
    {
        // TESTING:
        //Hamkin(i,i) = 1.5 + 0.25*nel0[i] + 0.75*nel1[i];
        
        int ielec = int(i/NHILBPHON); // whether we are in sector 0, 1, 2, 3
        Hamph(i,i) = OMEGA * (nph0[i]); // automatically diagonal in electrons, since we are in diagonal part
        
        Sz0(i,i) = 0.5*(nup0[i]-ndn0[i]);
        //Sz0(i,i) = 0.5*(nup1[i]-ndn1[i]);
        
        if(ielec == 0 || ielec == 3)
        {
            Hamu(i,i) = U1;
            Hamdocc(i,i) = 1.;
            Hamu0(i,i) = U0;
        }
        
        for(int j=0; j<NHILB; ++j)
        {
            int jelec = int(j/NHILBPHON);
            
          
            // new version: use expigee with Peierls
#ifndef SECONDORDER
            if(((ielec == 1 && jelec == 0) || (ielec == 2 && jelec == 0) || (ielec == 3 && jelec == 1) || (ielec == 3 && jelec == 2)) )
            {
                Hamkin(i,j) = -JHOP * expigee(nph0[i],nph0[j]);
                Hamhop(i,j) = -JHOP * expigee(nph0[i],nph0[j]);
            }
            
            if(((ielec == 0 && jelec == 1) || (ielec == 0 && jelec == 2) || (ielec == 1 && jelec == 3) || (ielec == 2 && jelec == 3)) )
            {
                Hamkin(i,j) = -JHOP * conj(expigee(nph0[i],nph0[j]));
                Hamhopconj(i,j) = -JHOP * conj(expigee(nph0[i],nph0[j]));
            }
            
#else
            
            
            
            if( ((ielec == 1 && jelec == 0) || (ielec == 0 && jelec == 1) || (ielec == 1 && jelec == 3) || (ielec == 3 && jelec == 1)) && nph0[i]==nph0[j])
            {
                Hamkin(i,j) = -JHOP * (1. - ASQUARED*G1*G1 * 0.5 * (2.*nph0[i] + 1.)); // correction from A^2 term
            }
            
            if( ((ielec == 2 && jelec == 0) || (ielec == 0 && jelec == 2) || (ielec == 2 && jelec == 3) || (ielec == 3 && jelec == 2)) && nph0[i]==nph0[j])
            {
                Hamkin(i,j) = -JHOP * (1. - ASQUARED*G1*G1 * 0.5 * (2.*nph0[i] + 1.));
             }
            
            
            if(((ielec == 1 && jelec == 0) || (ielec == 2 && jelec == 0) || (ielec == 3 && jelec == 1) || (ielec == 3 && jelec == 2)) )
            {
                if(nph0[i]==nph0[j]+1)
                    Hamg1(i,j) = G1 * (-II*JHOP) * sqrt(nph0[j]+1);
                if(nph0[i]==nph0[j]+2)
                    Hamg1(i,j) = ASQUARED*0.5*G1*G1 *JHOP * sqrt((nph0[j]+2)*(nph0[j]+1)); // A^2 term
            }
            
            if(((ielec == 0 && jelec == 1) || (ielec == 0 && jelec == 2) || (ielec == 1 && jelec == 3) || (ielec == 2 && jelec == 3)) )
            {
                if(nph0[i]==nph0[j]+1)
                    Hamg1(i,j) = G1 * (II*JHOP) * sqrt(nph0[j]+1);
                if(nph0[i]==nph0[j]+2)
                    Hamg1(i,j) = ASQUARED*0.5*G1*G1 *JHOP * sqrt((nph0[j]+2)*(nph0[j]+1)); // A^2 term
            }
            
            if(((ielec == 1 && jelec == 0) || (ielec == 2 && jelec == 0) || (ielec == 3 && jelec == 1) || (ielec == 3 && jelec == 2)))
            {
                if(nph0[i]==nph0[j]-1)
                    Hamg1(i,j) = G1 * (-II*JHOP) * sqrt(nph0[j]);
                if(nph0[i]==nph0[j]-2)
                    Hamg1(i,j) = ASQUARED*0.5*G1*G1 *JHOP * sqrt(nph0[j]*(nph0[j]-1)); // A^2 term
            }
            
            if(((ielec == 0 && jelec == 1) || (ielec == 0 && jelec == 2) || (ielec == 1 && jelec == 3) || (ielec == 2 && jelec == 3)))
            {
                if(nph0[i]==nph0[j]-1)
                    Hamg1(i,j) = G1 * (II*JHOP) * sqrt(nph0[j]);
                if(nph0[i]==nph0[j]-2)
                    Hamg1(i,j) = ASQUARED*0.5*G1*G1 *JHOP * sqrt(nph0[j]*(nph0[j]-1)); // A^2 term
            }
            
#endif
            
            // x terms for coupling to field
            if(nph0[i]==nph0[j]+1 && ielec == jelec)
            {
                Hamx(i,j) = sqrt(nph0[j]+1);
            }
            
            if(nph0[i]==nph0[j]-1 && ielec == jelec)
            {
                Hamx(i,j) = sqrt(nph0[j]); // automatically 0 for nph=0 state
            }
            
        }
        
    }
    
    for(int i=0; i<NHILB; ++i)
        for(int j=0; j<NHILB; ++j)
            Ham0(i,j) = Hamph(i,j) + Hamkin(i,j) + Hamu0(i,j) + Hamg1(i,j);
    
    /*
    Ham0 += Hamph;
    Ham0 += Hamkin;
    Ham0 += Hamg1;
    */
    
    
    cout << endl;
    
    /*
    cout << "Print Hamiltonian" << endl;
    
    ofstream outham;
    outham.open("ham.dat");
    for(int i=0; i<NHILB; ++i)
    {
        for(int j=0; j<NHILB; ++j)
        {
            outham << (Ham0(i,j)) << '\t';
        }
        outham << endl;
    }
    cout << endl;
    outham.close();
     */
    
    
    
    // REDUCED HILBERT SPACE FOR N-1 PARTICLES (1 electron, spin down, to fix it)
    // ADDED HILBERT SPACE FOR N+1 PARTICLES (2 up spins, 1 down spin remaining)
    
    vector<double> nel0red(NHILBREDUCED);
    vector<double> nel1red(NHILBREDUCED);
    
    vector<double> nel0added(NHILBREDUCED);
    vector<double> nel1added(NHILBREDUCED);
    
    // fermionic ordering to clarify signs: site 0 left, site 1 right; up left, down right
    // electrons |0 down ; 0 0>
    for(int n0=0; n0<NHILBPHON; ++n0)
        {
            nel0red[n0] = 1;
            nel1red[n0] = 0;
        }
    
    // electrons |0 0 ; 0 down>
    for(int n0=0; n0<NHILBPHON; ++n0)
        {
            nel0red[NHILBPHON + n0] = 0;
            nel1red[NHILBPHON + n0] = 1;
        }
    
    //hopping from left to right (-II*JHOP*G1): 10
    
    // fermionic ordering to clarify signs: site 0 left, site 1 right; up left, down right
    // electrons |up down ; up 0>
    for(int n0=0; n0<NHILBPHON; ++n0)
        {
            nel0added[n0] = 2;
            nel1added[n0] = 1;
        }
    
    // electrons |up 0 ; up down>
    for(int n0=0; n0<NHILBPHON; ++n0)
        {
            nel0added[NHILBPHON + n0] = 1;
            nel1added[NHILBPHON + n0] = 2;
        }
    
    //hopping from left to right (-II*JHOP*G1): 10
    
    cmat Ham0red(NHILBREDUCED, NHILBREDUCED);
    cmat Hamxred(NHILBREDUCED, NHILBREDUCED);
    
    cmat Hamhopred(NHILBREDUCED, NHILBREDUCED); // for classical Peierls phase
    cmat Hamhopconjred(NHILBREDUCED, NHILBREDUCED);
    
    cmat Ham0added(NHILBREDUCED, NHILBREDUCED);
    cmat Hamxadded(NHILBREDUCED, NHILBREDUCED);
    
    cmat Hamhopadded(NHILBREDUCED, NHILBREDUCED); // for classical Peierls phase
    cmat Hamhopconjadded(NHILBREDUCED, NHILBREDUCED);
    
    for(int i=0; i<NHILBREDUCED; ++i)
    {
        
        int ielec = int(i/NHILBPHON); // whether we are in sector 0, 1
        Ham0red(i,i) += OMEGA * (nph0[i]); // automatically diagonal in electrons, since we are in diagonal part; phonon occupations are independent of electron sector and also work in reduced space! :-)
        Ham0added(i,i) += OMEGA * (nph0[i]) + U1;
        
        for(int j=0; j<NHILBREDUCED; ++j)
        {
            int jelec = int(j/NHILBPHON); // whether we are in sector 0, 1
            
            
#ifndef SECONDORDER
            
            if((ielec == 1 && jelec == 0))
            {
                Ham0red(i,j) = -JHOP * expigee(nph0[i],nph0[j]);
                Ham0added(i,j) = -JHOP * expigee(nph0[i],nph0[j]);
                Hamhopred(i,j) = -JHOP * expigee(nph0[i],nph0[j]);
                Hamhopadded(i,j) = -JHOP * expigee(nph0[i],nph0[j]);
            }
            
            if((ielec == 0 && jelec == 1))
            {
                Ham0red(i,j) = -JHOP * conj(expigee(nph0[i],nph0[j]));
                Ham0added(i,j) = -JHOP * conj(expigee(nph0[i],nph0[j]));
                Hamhopconjred(i,j) = -JHOP * conj(expigee(nph0[i],nph0[j]));
                Hamhopconjadded(i,j) = -JHOP * conj(expigee(nph0[i],nph0[j]));
            }
            
#else
            if( ((ielec == 1 && jelec == 0) || (ielec == 0 && jelec == 1)) && nph0[i]==nph0[j])
            {
                Ham0red(i,j) = -JHOP  * (1. - ASQUARED* G1*G1 * 0.5 * (2.*nph0[i] + 1.));
                Ham0added(i,j) = -JHOP  * (1. - ASQUARED* G1*G1 * 0.5 * (2.*nph0[i] + 1.));
            }
            
            if((ielec == 1 && jelec == 0))
            {
                if(nph0[i]==nph0[j]+1)
                {
                    Ham0red(i,j) = G1 * (-II*JHOP) * sqrt(nph0[j]+1);
                    Ham0added(i,j) = G1 * (-II*JHOP) * sqrt(nph0[j]+1);
                }
                
                if(nph0[i]==nph0[j]+2)
                {
                    Ham0red(i,j) = ASQUARED* 0.5*G1*G1 *JHOP * sqrt((nph0[j]+2)*(nph0[j]+1)); // A^2 term
                    Ham0added(i,j) = ASQUARED* 0.5*G1*G1 *JHOP * sqrt((nph0[j]+2)*(nph0[j]+1)); // A^2 term
                }
            }
            
            if((ielec == 1 && jelec == 0))
            {
                if(nph0[i]==nph0[j]-1)
                {
                    Ham0red(i,j) = G1 * (-II*JHOP) * sqrt(nph0[j]);
                    Ham0added(i,j) = G1 * (-II*JHOP) * sqrt(nph0[j]);
                }
                
                if(nph0[i]==nph0[j]-2)
                {
                    Ham0red(i,j) = ASQUARED* 0.5*G1*G1 *JHOP * sqrt(nph0[j]*(nph0[j]-1)); // A^2 term
                    Ham0added(i,j) = ASQUARED* 0.5*G1*G1 *JHOP * sqrt(nph0[j]*(nph0[j]-1)); // A^2 term
                }
            }
            
            if((ielec == 0 && jelec == 1))
            {
                if(nph0[i]==nph0[j]+1)
                {
                    Ham0red(i,j) = G1 * (II*JHOP) * sqrt(nph0[j]+1);
                    Ham0added(i,j) = G1 * (II*JHOP) * sqrt(nph0[j]+1);
                }
                
                if(nph0[i]==nph0[j]+2)
                {
                    Ham0red(i,j) = ASQUARED* 0.5*G1*G1 *JHOP * sqrt((nph0[j]+2)*(nph0[j]+1)); // A^2 term
                    Ham0added(i,j) = ASQUARED* 0.5*G1*G1 *JHOP * sqrt((nph0[j]+2)*(nph0[j]+1)); // A^2 term
                }
            }
            
            if((ielec == 0 && jelec == 1))
            {
                if(nph0[i]==nph0[j]-1)
                {
                    Ham0red(i,j) = G1 * (II*JHOP) * sqrt(nph0[j]);
                    Ham0added(i,j) = G1 * (II*JHOP) * sqrt(nph0[j]);
                }
                
                if(nph0[i]==nph0[j]-2)
                {
                    Ham0red(i,j) = ASQUARED* 0.5*G1*G1 *JHOP * sqrt(nph0[j]*(nph0[j]-1)); // A^2 term
                    Ham0added(i,j) = ASQUARED* 0.5*G1*G1 *JHOP * sqrt(nph0[j]*(nph0[j]-1)); // A^2 term
                }
            }
#endif
            
            if(nph0[i]==nph0[j]+1 && ielec == jelec)
            {
                Hamxred(i,j) = sqrt(nph0[j]+1);
                Hamxadded(i,j) = sqrt(nph0[j]+1);
            }
            
            if(nph0[i]==nph0[j]-1 && ielec == jelec)
            {
                Hamxred(i,j) = sqrt(nph0[j]); // automatically 0 for nph=0 state
                Hamxadded(i,j) = sqrt(nph0[j]); // automatically 0 for nph=0 state
            }
            
        }
        
    }
    
    
    
    cmat Annihilator(NHILBREDUCED, NHILB); // operator that produces N-1 particle wave function from N particle wave function, annihilating up-electron on site 0
    for(int i=0; i<NHILBREDUCED; ++i)
    {
        //int ielec = int(i/NHILBPHON); // whether we are in sector 0, 1
        for(int j=0; j<NHILB; ++j)
        {
            //int jelec = int(j/NHILBPHON); // whether we are in sector 0, 1, 2, 3
            if(i==j)
                Annihilator(i,j) = 1; // really that simple ... note that this is not a diagonal matrix
        }
    }
    
    
    
    // check sign of hopping for added term above
    // sectors are 21, 12
    // sign is "-" ==> +JHOP
    
    
    cmat Creator(NHILBREDUCED, NHILB); // operator that produces N+1 particle wave function from N particle wave function, creating up-electron on site 0
    for(int i=0; i<NHILBREDUCED; ++i)
    {
        //int ielec = int(i/NHILBPHON); // whether we are in sector 0, 1
        for(int j=0; j<NHILB; ++j)
        {
            //int jelec = int(j/NHILBPHON); // whether we are in sector 0, 1, 2, 3
            if(i==j-2*NHILBPHON)
                Creator(i,j) = 1; // really that simple ... note that this is not a diagonal matrix // we are mapping 0 => null, 1 => null, 2 => 0, 3 => 1
        }
    }
    
    // END CONSTRUCTION FOR REDUCED HILBERT SPACE
    
    
    
    // diagonalize Ham0 to find ground state
    cmat Dum(NHILB, NHILB);
    vector<double> evals(NHILB);
    for(int i=0; i<NHILB; ++i)
        for(int j=0; j<NHILB; ++j)
            Dum(i,j) = Ham0(i,j);
    
    diagonalize(Dum, evals, NHILB);
    
    double groundstate_energy = evals[0];
    
    // print ground state energy
    if(myrank==0)
        cout << "lowest energies = " << evals[0] << ", " << evals[1] << endl;
    // print ground state energy
    if(myrank==0)
    {
        for(int i=0; i<NHILB; ++i)
            cout << "groundstate vector = " << Dum(i,0) << endl;
    }
    
    // check if ground state is correct
    vector<cdouble> groundstate(NHILB);
    for(int i=0; i<NHILB; ++i)
    {
        groundstate[i] = Dum(i,0);
    }
    
    vector<cdouble> Hgroundstate(NHILB);
    for(int i=0; i<NHILB; ++i)
        for(int j=0; j<NHILB; ++j)
            Hgroundstate[i] += Ham0(i,j) * groundstate[j];
    
    double energy = 0;
    for(int i=0; i<NHILB; ++i)
        energy += (conj(groundstate[i]) * Hgroundstate[i]).real();
    
    if(myrank==0)
        cout << "ground state energy checked = " << energy << endl;
    
    double dtstep = TMAX/NUMT;
    
    vector<cdouble> psi0(NHILB);
    
    //initialize with groundstate
    /*
    for(int i=0; i<NHILB; ++i)
        psi0[i] = groundstate[i];
     */
    
    //initialize with electronic groundstate x coherent state |alpha>
    
    // diagonalize Hamelectron to find electronic round state
    int NHILBELECTRON = 4;
    cmat Hamelectron(NHILBELECTRON, NHILBELECTRON);
    vector<double> evalselectron(NHILBELECTRON);
    
    for(int i=0; i<NHILBELECTRON; ++i)
        for(int j=0; j<NHILBELECTRON; ++j)
            Hamelectron(i,j) = 0.;
    
    Hamelectron(0,0) = U0;
    Hamelectron(3,3) = U0;
    Hamelectron(0,1) = -JHOP;
    Hamelectron(1,0) = -JHOP;
    Hamelectron(0,2) = -JHOP;
    Hamelectron(2,0) = -JHOP;
    Hamelectron(1,3) = -JHOP;
    Hamelectron(3,1) = -JHOP;
    Hamelectron(2,3) = -JHOP;
    Hamelectron(3,2) = -JHOP;
    
    diagonalize(Hamelectron, evalselectron, NHILBELECTRON);
    
    vector<cdouble> electronicgroundstate(NHILBELECTRON);
    for(int i=0; i<NHILBELECTRON; ++i)
    {
        electronicgroundstate[i] = Hamelectron(i,0);
    }
    
    double normsquared = 0.;
    for(int i=0; i<NHILB; ++i)
    {
        psi0[i] = 0.;
        int m = i%NHILBPHON; // photonic sector
        int n = i/NHILBPHON; // electronic sector
        cdouble value = exp(-ALPHA*ALPHA/2.) * pow(ALPHA, m)/sqrt(faculty(m)) * electronicgroundstate[n];
        psi0[i] = value;
        normsquared += abs(conj(value)*value);
    }
    for(int i=0; i<NHILB; ++i)
        psi0[i] /= sqrt(normsquared);
    
    vector<cdouble> psi1(NHILB);
    cmat A1(NHILB,NHILB);
    cmat A2(NHILB,NHILB);
    
    vector<double> tlist(NUMT+1);
    for(int it=0; it<=NUMT; ++it)
        tlist[it] = it*dtstep;
    
    
    for(int i=0; i<NHILB; ++i)
        for(int j=0; j<NHILB; ++j)
            Ham0(i,j) = Hamph(i,j) + Hamkin(i,j) + Hamu(i,j) + Hamg1(i,j);
    
    ofstream outfile;
    ofstream outarpes;
    ofstream outspin;
    ofstream outarpese;
    
    if(myrank==0)
    {
        outfile.open("out.dat");
        outfile << setw(4) << setprecision(15);
        outfile << "# time" << '\t' << "ekin" << '\t' << "epot" << '\t' << "ekin+epot" << '\t' << "ephon" << '\t' << "etot" << '\t' << "x(t)" << '\t' << "field" << '\t' << "docc" << endl;
    }
    
    if(myrank==0)
    {
        outarpes.open("arpes.dat");
        outarpes << setw(4) << setprecision(15);
        outarpes << "# center time TARPES = " << '\t' << TARPES << endl;
        outarpes << "# omega" << '\t' << "arpes" << '\t' << "retarded" << endl;
    }
    
    if(myrank==0)
    {
        outspin.open("spin.dat");
        outspin << setw(4) << setprecision(15);
        outspin << "# omega" << '\t' << "<Sz0 Sz0>(omega)" << endl;
    }
    
    if(myrank==0)
    {
        outarpese.open("arpesequil.dat");
        outarpese << setw(4) << setprecision(15);
        outarpese << "# omega" << '\t' << "arpes" << '\t' << "retarded" << endl;
    }
    
    vector<cdouble> psi0annil(NHILBREDUCED);
    vector<cdouble> psi0creat(NHILBREDUCED);
    for(int i=0; i<NHILBREDUCED; ++i)
    {
        psi0annil[i] = 0.;
        psi0creat[i] = 0.;
        for(int j=0; j<NHILB; ++j)
        {
            psi0annil[i] += Annihilator(i,j) * psi0[j];
            psi0creat[i] += Creator(i,j) * psi0[j];
        }
    }
    
    double eta = 0.25/(2.*SIGARPES); // width for broadening
    double dom = (OMEGAMAX-OMEGAMIN)/(NOMEGAARPES);
    
    for(int iw = 0; iw <= NOMEGAARPES; ++iw)
    {
        cdouble arpeseq = 0.;
        cdouble retardedeq = 0.;
        double omegaarpes = OMEGAMIN + dom * iw;
        cmat resolvent(NHILBREDUCED, NHILBREDUCED);
        cmat resolvent2(NHILBREDUCED, NHILBREDUCED);
        
        for(int i=0; i<NHILBREDUCED; ++i)
        {
            resolvent(i,i) = omegaarpes + II * eta - groundstate_energy; // important to add E_0 here
            resolvent2(i,i) = omegaarpes - II * eta + groundstate_energy;
            for(int j=0; j<NHILBREDUCED; ++j)
            {
                resolvent(i,j) += Ham0red(i,j);
                resolvent2(i,j) -= Ham0added(i,j);
            }
        }
        
        // invert w + i eta - H
        invert(resolvent.ptr(), NHILBREDUCED);
        // invert w - i eta + H
        invert(resolvent2.ptr(), NHILBREDUCED);
        
        // take trace with annihilated state
        for(int i=0; i<NHILBREDUCED; ++i)
        {
            cdouble psidum = 0.;
            cdouble psidum2 = 0.;
            for(int j=0; j<NHILBREDUCED; ++j)
            {
                psidum += resolvent(i,j)*psi0annil[j];
                psidum2 -= resolvent2(i,j)*psi0creat[j];
            }
            arpeseq += conj(psi0annil[i]) * psidum;
            retardedeq += conj(psi0annil[i]) * psidum + conj(psi0creat[i]) * psidum2;
        }
        
        arpeseq *= -2./M_PI;
        retardedeq *= -2./M_PI;
        
        if(myrank==0) outarpese << omegaarpes << '\t' << imag(arpeseq) << '\t' << imag(retardedeq) << endl;
        
    }
    
    
    // FOR ARPES PREPS
    // need to store wavefunction at ARPES time points
    vector<double> arpestlist(2*NTARPES+1);
    vector<int> tmapping(2*NTARPES+1); // maps to actual propagation time points; careful, this requires overlapping grids
    double dtarpes = 6.*SIGARPES/(2*NTARPES);
    for(int it=-NTARPES; it<=NTARPES; ++it)
    {
        arpestlist[it+NTARPES] = double(it)*dtarpes + TARPES; // TARPES is the central time
        tmapping[it+NTARPES] = int(arpestlist[it+NTARPES]/dtstep);
        //if(myrank==0) cout << "ARPES step: " << it+NTARPES << " , actual time step: " << tmapping[it+NTARPES] << endl;
    }
    
    // wavefunction storage // only needed in reduced space!!!
    cmat psireduced(2*NTARPES+1, NHILBREDUCED);
    // this is for the addition of a particle in order to compute the addition spectrum
    cmat psiadded(2*NTARPES+1, NHILBREDUCED);
    // this is for spin-spin, full Hilbert space:
    cmat psispin(2*NTARPES+1, NHILB);
    // END FOR ARPES PREPS
    
    
    // time stepping
    for(int it=1; it<=NUMT; ++it)
    {
        cmat U(NHILB,NHILB);
        
        double time1 = tlist[it-1] + (0.5-sqrt(3.)/6.) * dtstep;
        double time2 = tlist[it-1] + (0.5+sqrt(3.)/6.) * dtstep;
        

        cdouble peierls1 = exp(II*AMAX * sin(OMEGAPUMP * time1)) - 1.; // difference to undriven
        cdouble peierls2 = exp(II*AMAX * sin(OMEGAPUMP * time2)) - 1.;
        cdouble peierls = exp(II*AMAX * sin(OMEGAPUMP * tlist[it])) - 1.;
        // need to define kinetic energy terms time-dependent in here...
        
        
        
        double field1 = FMAX * sin(OMEGAPUMP * time1);
        double field2 = FMAX * sin(OMEGAPUMP * time2);
        double field = FMAX * sin(OMEGAPUMP * tlist[it]);

        
/*
        double field1 = FMAX * sin(OMEGAPUMP * (time1-FCENTER)) * shape(time1);
        double field2 = FMAX * sin(OMEGAPUMP * (time2-FCENTER)) * shape(time1);
        double field = FMAX * sin(OMEGAPUMP * (tlist[it]-FCENTER)) * shape(tlist[it]);
*/
        
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
            {
                A1(i,j) = Ham0(i,j) + field1 * Hamx(i,j) + peierls1 * Hamhop(i,j) + conj(peierls1) * Hamhopconj(i,j);
                A2(i,j) = Ham0(i,j) + field2 * Hamx(i,j) + peierls2 * Hamhop(i,j) + conj(peierls1) * Hamhopconj(i,j);
            }

        
        set_U_step(U, A1, A2, evals, NHILB, dtstep);
        
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                psi1[i] += U(i,j) * psi0[j];
        
        for(int i=0; i<NHILB; ++i)
        {
            psi0[i] = psi1[i];
            psi1[i] = 0.;
        }
        
        
        // store ARPES wavefunction; this is very bad programming since I go thru the whole loop every time
        for(int ita=-NTARPES; ita<=NTARPES; ++ita)
        {
            if(tmapping[ita+NTARPES] == it) // times matching at end of propagation step
            {
                for(int i=0; i<NHILBREDUCED; ++i)
                {
                    psireduced(ita+NTARPES, i) = 0;
                    psiadded(ita+NTARPES, i) = 0;
                    for(int j=0; j<NHILB; ++j)
                    {
                        psireduced(ita+NTARPES, i) += Annihilator(i,j) * psi0[j];
                        psiadded(ita+NTARPES, i) += Creator(i,j) * psi0[j];
                    }
                }
                
                for(int i=0; i<NHILB; ++i)
                {
                    psispin(ita+NTARPES, i) = 0;
                    for(int j=0; j<NHILB; ++j)
                        psispin(ita+NTARPES, i) += Sz0(i,j) * psi0[j];
                }
            }
        }
         
        
        vector<cdouble> Hkinpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Hkinpsi0[i] += Hamkin(i,j) * psi0[j];
        
        double ekin = 0;
        for(int i=0; i<NHILB; ++i)
            ekin += (conj(psi0[i]) * Hkinpsi0[i]).real();
        
        vector<cdouble> Hpotpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Hpotpsi0[i] += Hamu(i,j) * psi0[j];
        
        double epot = 0;
        for(int i=0; i<NHILB; ++i)
            epot += (conj(psi0[i]) * Hpotpsi0[i]).real();
        
        //double occ.
        vector<cdouble> Hdoccpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Hdoccpsi0[i] += Hamdocc(i,j) * psi0[j];
        
        double docc = 0;
        for(int i=0; i<NHILB; ++i)
            docc += (conj(psi0[i]) * Hdoccpsi0[i]).real();
        
        vector<cdouble> Hphonpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Hphonpsi0[i] += Hamph(i,j) * psi0[j];
        
        double ephon = 0;
        for(int i=0; i<NHILB; ++i)
            ephon += (conj(psi0[i]) * Hphonpsi0[i]).real();
        
        vector<cdouble> Htotpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Htotpsi0[i] += Ham0(i,j) * psi0[j];
        
        double etot = 0;
        for(int i=0; i<NHILB; ++i)
            etot += (conj(psi0[i]) * Htotpsi0[i]).real();
        
        vector<cdouble> Hxpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Hxpsi0[i] += Hamx(i,j) * psi0[j];
        
        double xt = 0;
        for(int i=0; i<NHILB; ++i)
            xt += (conj(psi0[i]) * Hxpsi0[i]).real();
        
        if(myrank==0)
        {
            /*
            cout << "time = " << tlist[it];
            cout << ", kinetic energy = " << ekin;
            cout << ", Hubbard energy = " << epot;
            cout << ", electronic energy = " << ekin+epot;
            cout << ", phononic energy = " << ephon;
            cout << ", x(t) = " << xt;
            cout << ", total energy = " << etot;
            cout << ", double occ = " << docc << endl;
             */
            
            outfile << tlist[it] << '\t' << ekin << '\t' << epot << '\t' << ekin+epot << '\t' << ephon << '\t' << etot << '\t' << xt << '\t' << field << '\t' << docc << endl;
        }
        
    }
    
    
    // construction for spectrum: need to store wave functions at some predetermined number of time steps grid (reduced sampling?!) - maybe for 50 times or so, depending on probe width
    // then apply Annihilator and propagate using reduced Hilbert space Hamiltonian, accumulate directly to spectrum
    // fix the center of the probe window!!!
    // propagate only for half the times
    
    // now propagate the reduced wavefunction in reduced Hilbert space
    
    
    vector<cdouble> psi0reduced(NHILBREDUCED);
    vector<cdouble> psi1reduced(NHILBREDUCED);
    cmat A1reduced(NHILBREDUCED,NHILBREDUCED);
    cmat A2reduced(NHILBREDUCED,NHILBREDUCED);
    vector<double> evalsreduced(NHILBREDUCED);
    
    vector<cdouble> psi0added(NHILBREDUCED);
    vector<cdouble> psi1added(NHILBREDUCED);
    cmat A1added(NHILBREDUCED,NHILBREDUCED);
    cmat A2added(NHILBREDUCED,NHILBREDUCED);
    vector<double> evalsadded(NHILBREDUCED);
    
    // need to also propagate spin-measured wavefunction
    vector<cdouble> psi0spin(NHILB);
    vector<cdouble> psi1spin(NHILB);
    cmat A1spin(NHILB,NHILB);
    cmat A2spin(NHILB,NHILB);
    vector<double> evalsspin(NHILB);
    
    double shaper = 0.;
    vector<double> arpessignal(NOMEGAARPES+1);
    vector<double> spinsignal(NOMEGAARPES+1);
    vector<double> retardedsignal(NOMEGAARPES+1);
    double domega = (OMEGAMAX-OMEGAMIN)/(NOMEGAARPES);
    
    if(myrank==0) cout << "Starting ARPES postprocessing" << endl;
    
    for(int it=-NTARPES+myrank; it<=NTARPES; it+=numprocs)
    {
        // start propagation from respective reference state at (t,t)
        // only propagate forward in time
        
        if(myrank==0) cout << "ARPES at time step " << it+NTARPES << " out of " << 2*NTARPES+1 << endl;
        
        for(int i=0; i<NHILBREDUCED; ++i)
        {
            psi0reduced[i] = psireduced(it+NTARPES, i);
            psi0added[i] = psiadded(it+NTARPES, i);
        }
        for(int i=0; i<NHILB; ++i)
        {
            psi0spin[i] = psispin(it+NTARPES, i);
        }
        
        cmat Ureduced(NHILBREDUCED, NHILBREDUCED);
        cmat Uadded(NHILBREDUCED, NHILBREDUCED);
        cmat Uspin(NHILB, NHILB);

        for(double time=arpestlist[it+NTARPES]; time<arpestlist[2*NTARPES]; time += dtstep)
        {
            double time1 = time + (0.5-sqrt(3.)/6.) * dtstep;
            double time2 = time + (0.5+sqrt(3.)/6.) * dtstep;
            double field1 = FMAX * sin(OMEGAPUMP * time1);
            double field2 = FMAX * sin(OMEGAPUMP * time2);
            cdouble peierls1 = exp(II*AMAX * sin(OMEGAPUMP * time1)) - 1.; //difference to undriven
            cdouble peierls2 = exp(II*AMAX * sin(OMEGAPUMP * time2)) - 1.;
            
            for(int i=0; i<NHILBREDUCED; ++i)
                for(int j=0; j<NHILBREDUCED; ++j)
                {
                    A1reduced(i,j) = Ham0red(i,j) + field1 * Hamxred(i,j) + peierls1 * Hamhop(i,j) + conj(peierls1) * Hamhopconj(i,j);
                    A2reduced(i,j) = Ham0red(i,j) + field2 * Hamxred(i,j) + peierls2 * Hamhopred(i,j) + conj(peierls1) * Hamhopconjred(i,j);
                    
                    A1added(i,j) = Ham0added(i,j) + field1 * Hamxadded(i,j) + peierls1 * Hamhop(i,j) + conj(peierls1) * Hamhopadded(i,j);
                    A2added(i,j) = Ham0added(i,j) + field2 * Hamxadded(i,j) + peierls2 * Hamhop(i,j) + conj(peierls1) * Hamhopconjadded(i,j);
                }
            
            for(int i=0; i<NHILB; ++i)
                for(int j=0; j<NHILB; ++j)
                {
                    A1spin(i,j) = Ham0(i,j) + field1 * Hamx(i,j)  + peierls1 * Hamhop(i,j) + conj(peierls1) * Hamhopconj(i,j);
                    A2spin(i,j) = Ham0(i,j) + field2 * Hamx(i,j) + peierls1 * Hamhop(i,j) + conj(peierls1) * Hamhopconj(i,j);
                }
            
            set_U_step(Ureduced, A1reduced, A2reduced, evalsreduced, NHILBREDUCED, dtstep);
            set_U_step(Uadded, A1added, A2added, evalsadded, NHILBREDUCED, dtstep);
            set_U_step(Uspin, A1spin, A2spin, evalsspin, NHILB, dtstep);
            
            for(int i=0; i<NHILBREDUCED; ++i)
                for(int j=0; j<NHILBREDUCED; ++j)
                {
                    psi1reduced[i] += Ureduced(i,j) * psi0reduced[j];
                    psi1added[i] += Uadded(i,j) * psi0added[j];
                }
            
            for(int i=0; i<NHILB; ++i)
                for(int j=0; j<NHILB; ++j)
                {
                    psi1spin[i] += Uspin(i,j) * psi0spin[j];
                }
            
            for(int i=0; i<NHILBREDUCED; ++i)
            {
                psi0reduced[i] = psi1reduced[i];
                psi1reduced[i] = 0.;
                
                psi0added[i] = psi1added[i];
                psi1added[i] = 0.;
            }
            
            for(int i=0; i<NHILB; ++i)
            {
                psi0spin[i] = psi1spin[i];
                psi1spin[i] = 0.;
            }
            
            // accumulate to ARPES signal at t'-t when congruent with arpestlist
            // use symmetry for other half of the t'-t triangle
            for(int ittest=-NTARPES; ittest<=NTARPES; ++ittest)
            {
                if(arpestlist[ittest+NTARPES] == time+dtstep) // the time to which we have just propagated
                {
                    // compute overlap
                    cdouble overlap = 0.;
                    cdouble overlapadded = 0.;
                    cdouble overlapspin = 0.;
                    for(int i=0; i<NHILBREDUCED; ++i)
                    {
                        overlap += conj(psireduced(ittest+NTARPES, i)) * psi0reduced[i];
                        overlapadded += conj(psiadded(ittest+NTARPES, i)) * psi0added[i];
                    }
                    
                    for(int i=0; i<NHILB; ++i)
                    {
                        overlapspin += conj(psispin(ittest+NTARPES, i)) * psi0spin[i];
                    }
                    
                    // add to ARPES
                    
                    for(int iw = 0; iw <= NOMEGAARPES; ++iw)
                    {
                        double omegaarpes = OMEGAMIN + domega * iw;
                        shaper = arpesshape(arpestlist[ittest+NTARPES], arpestlist[it+NTARPES], TARPES, SIGARPES);
                        
                        
                        arpessignal[iw] += 2.*real(exp(II * omegaarpes * (arpestlist[it+NTARPES] - arpestlist[ittest+NTARPES])) * overlap) *  dtarpes * dtarpes * shaper;
                        
                        retardedsignal[iw] += 2.*real(exp(II * omegaarpes * (arpestlist[ittest+NTARPES] - arpestlist[it+NTARPES])) * overlapadded) *  dtarpes * dtarpes * shaper;
                        
                        retardedsignal[iw] += 2.*real(exp(II * omegaarpes * (arpestlist[it+NTARPES] - arpestlist[ittest+NTARPES])) * overlap) *  dtarpes * dtarpes * shaper;
                        
                        spinsignal[iw] += 1.*real(exp(II * omegaarpes * (arpestlist[it+NTARPES] - arpestlist[ittest+NTARPES])) * overlapspin) *  dtarpes * dtarpes * shaper;
                        
                        spinsignal[iw] += 1.*real(exp(-II * omegaarpes * (arpestlist[it+NTARPES] - arpestlist[ittest+NTARPES])) * overlapspin) *  dtarpes * dtarpes * shaper;
                    }
                }
            }
            
            
            
        }
            
        cdouble overlapdiag = 0.; // not necessarily = 1 in general because it contains info about particle number
        
        cdouble overlapdiagadded = 0.;
        
        cdouble overlapdiagspin = 0.;
        
        for(int i=0; i<NHILBREDUCED; ++i)
        {
            overlapdiag += conj(psireduced(it+NTARPES, i)) * psireduced(it+NTARPES, i);
            overlapdiagadded += conj(psiadded(it+NTARPES, i)) * psiadded(it+NTARPES, i);
        }
        
        for(int i=0; i<NHILB; ++i)
            overlapdiagspin += conj(psispin(it+NTARPES, i)) * psispin(it+NTARPES, i);

        
        // add diagonal t-t term at the end
        shaper = arpesshape(arpestlist[it+NTARPES], arpestlist[it+NTARPES], TARPES, SIGARPES);
        for(int iw = 0; iw <= NOMEGAARPES; ++iw)
        {
            arpessignal[iw] += real(overlapdiag) * dtarpes * dtarpes * shaper ;
            retardedsignal[iw] += real(overlapdiag) * dtarpes * dtarpes * shaper ;
            retardedsignal[iw] += real(overlapdiagadded) * dtarpes * dtarpes * shaper ;
            spinsignal[iw] += real(overlapdiagspin) * dtarpes * dtarpes * shaper ;
        }
        
    }
    
#ifndef NO_MPI
    MPI_Allreduce(MPI_IN_PLACE, &arpessignal[0], NOMEGAARPES+1,
                  MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE, &retardedsignal[0], NOMEGAARPES+1,
                  MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE, &spinsignal[0], NOMEGAARPES+1,
                  MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif
    
    for(int iw = 0; iw <= NOMEGAARPES; ++iw)
        if(myrank==0) outarpes << OMEGAMIN + domega * iw << '\t' << arpessignal[iw] << '\t' << retardedsignal[iw] << endl;
    
    for(int iw = 0; iw <= NOMEGAARPES; ++iw)
        if(myrank==0) outspin << OMEGAMIN + domega * iw << '\t' << spinsignal[iw] << endl;
    
    if(myrank==0)
    {
        outarpes.close();
        outspin.close();
        outarpese.close();
        cout << "\n ... done! \n"<<flush;
    }
    
    
#ifndef NO_MPI
	MPI_Finalize();
#endif
    
    return 0;

}

