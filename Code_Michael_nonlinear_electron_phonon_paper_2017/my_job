###############################################################
#example SGE batch script for a hybrid MPI/OpenMP program using
#512 cores (16 nodes x 32 cores per node) with 2 MPI tasks per
#node and 16 OpenMP threads per MPI task 
#executed under the intel runtime 
###############################################################

## run in /bin/bash
#$ -S /bin/bash
## do not join stdout and stderr
#$ -j n
## name of the job
#$ -N test_hybrid
## execute job from the current working directory
#$ -cwd
## do not send mail
#$ -m n
## request 1 node (x 32 cores), must be a multiple of 32  
#$ -pe impi_hydra 32 
## run for xx minutes
#$ -l h_rt=04:00:00


module load intel impi mkl
export LD_LIBRARY_PATH=/afs/@cell/common/soft/intel/ics2015/15.0/mkl/lib/intel64/

#16 threads per MPI task => 2 MPI tasks per node
export OMP_NUM_THREADS=1

#relevant for OpenMP Fortran programs with large arrays allocated on the stack
#the default is just a few MB. If setting is too low typically segfaults are
#produced 
export KMP_STACKSIZE=256M

#pin threads to physical cores, hyperthreading is disabled by OS, verbose!
export KMP_AFFINITY="verbose,compact,granularity=core"

#portable GCC equivalents:
#export OMP_PROC_BIND=TRUE
#export OMP_DISPLAY_ENV=VERBOSE
#export OMP_PLACES="cores"
#export OMP_STACKSIZE=256M


TASKS=$(($NSLOTS/$OMP_NUM_THREADS))
PERHOST=$((32/$OMP_NUM_THREADS))

## to save memory at high core counts, the "connectionless user datagram protocol" 
## can be enabled (might come at the expense of speed)
## see https://software.intel.com/en-us/articles/dapl-ud-support-in-intel-mpi-library
# export I_MPI_DAPL_UD=1

##gather MPI statistics to be analyzed with itac's mps tool
# export I_MPI_STATS=all

##gather MPI debug information (high verbosity)
# export I_MPI_DEBUG=5

mpiexec -n $TASKS -perhost $PERHOST ./ed.x  
