// code to solve small electron-phonon problem with two electrons on two sites
// add option to compute spectral function from photoemission formula

// new version: with retarded spectrum and eventually also pair-pair correlations

#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
#include <vector>
#include <assert.h>
#include <math.h>
#include "matrix.h"
#ifndef NO_MPI
    #include <mpi.h>
#endif


using namespace std;

typedef complex<double> cdouble;
typedef matrix<cdouble> cmat;
cdouble II(0,1);

#define NSITES      2
#define NPHON       15
#define NHILB       900
#define NHILBPHON   225
#define NHILBREDUCED   450 // for spectrum, need N-1 particle space
#define OMEGA       0.5
#define OMEGAPUMP   0.50
#define JHOP        0.15
#define G1          0.0
#define G2          -0.05
#define U1          0.0 // Hubbard U
#define U0          0.0 // different from U1: quench U0 -> U1
#define FMAX        0.30
#define CWDRIVE     // if defined use cw drive; else use pulse
#define FWIDTH      3.0
#define FCENTER     15.0
#define TMAX        50.
#define NUMT        50

// for spectrum
#define TARPES      25.0
#define SIGARPES    8.0  // sigma for ARPES = probe duration
#define NTARPES     24  // 2*NTARPES+1 = number of time points for ARPES around TARPES = 6 sigma window
#define NOMEGAARPES 400
#define OMEGAMIN    -2.50
#define OMEGAMAX    1.50

// LAPACK stuff

extern "C" void zgetrf_(int*, int*, cdouble*, int*, int*, int*);
extern "C" void zgetri_(int*, cdouble*, int*, int*, cdouble*, int*, int*);
void invert(cdouble* G, int msize)
{
    int LWORK = msize, INFO=0;
    cdouble work[NHILB]; // use maximal size here
    assert(NHILB>=msize);
    int permutations[msize];
    
    zgetrf_(&msize, &msize, G, &msize, permutations, &INFO);
    if(INFO != 0)
    {
        cout << "Complex matrix inverse: Error at zgetrf. INFO: " << INFO<< endl;
    }
    
    zgetri_(&msize, G, &msize, permutations, work, &LWORK, &INFO);
    if(INFO != 0)
    {
        cout << "Complex matrix inverse: Error at zgetri. INFO: " << INFO<< endl;
    }
    
}

extern "C" void zheev_(char* jobz, char* uplo, int* N, cdouble* H, int* LDA, double* W, cdouble* work, int* lwork, double* rwork, int *info);

void diagonalize(cmat &H, vector<double> &evals, int N)
{
    int     matsize = N;
    int     lwork = 2*(2*N-1);
    double  rwork[3*N-2];
    cdouble work[2*(2*NHILB-1)]; // use maximal size here
    char    jobz = 'V';
    char    uplo = 'U';
    int     info;
    
	zheev_(&jobz, &uplo, &matsize, H.ptr(), &matsize, &evals[0], &work[0], &lwork, &rwork[0], &info);
	assert(!info);
}

void times(cmat &A, cmat &B, cmat &C, int N)
{
    for(int i=0; i<N; ++i)
        for(int j=0; j<N; ++j)
        {
            C(i,j) = 0.;
            for(int k=0; k<N; ++k)
            {
                C(i,j) += A(i,k)*B(k,j);
            }
        }
}

void set_U_step(cmat &U, cmat &A1, cmat &A2, vector<double> &evals, int N, double dtstep)
{
    cmat tmp1(N, N);
    cmat tmp2(N, N);
    
    cmat B1(N, N);
    cmat B2(N, N);
    
    double c1 = (3.-2.*sqrt(3.))/12.;
    double c2 = (3.+2.*sqrt(3.))/12.;
    
    for(int i=0; i < N; i++)
        for(int j=0; j < N; j++)
        {
            B1(i,j) = c1*A1(i,j) + c2*A2(i,j);
            B2(i,j) = c2*A1(i,j) + c1*A2(i,j);
        }
    
    
    diagonalize(B1, evals, N);
    for(int i=0; i < N; i++)
    {
        for(int j=0; j < N; j++)
        {
            tmp1(i,j) = 0.;
            for(int k=0; k < N; k++)
            {
                tmp1(i,j) += B1(i,k) * exp(-II * dtstep * evals[k]) * conj(B1(j,k)) ;
            }
        }
    }
    
    diagonalize(B2, evals, N);
    for(int i=0; i < N; i++)
    {
        for(int j=0; j < N; j++)
        {
            tmp2(i,j) = 0.;
            for(int k=0; k < N; k++)
            {
                tmp2(i,j) += B2(i,k) * exp(-II * dtstep * evals[k]) * conj(B2(j,k)) ;
            }
        }
    }
    
    times(tmp1, tmp2, U, N);
}

inline double shape(double time)
{
    return exp(-(time-FCENTER)*(time-FCENTER)/(2.*FWIDTH*FWIDTH));
}

inline double arpesshape(double time1, double time2, double time0, double sigma)
{
    return exp(-(time1-time0)*(time1-time0)/(2.*sigma*sigma)) * exp(-(time2-time0)*(time2-time0)/(2.*sigma*sigma)) / (sigma*2.*M_PI); // only one sigma in denominator for norm
}

int main(int argc, char * argv[])
{
    //************** MPI INIT ***************************
  	int numprocs=1, myrank=0, namelen;
    
#ifndef NO_MPI
  	char processor_name[MPI_MAX_PROCESSOR_NAME];
  	MPI_Init(&argc, &argv);
  	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  	MPI_Get_processor_name(processor_name, &namelen);
    
	MPI_Barrier(MPI_COMM_WORLD);
    
	MPI_Datatype ttype  = MPI_DOUBLE;
    
#endif
	if(myrank==0) cout << "\n\tProgram running on " << numprocs << " processors." << endl;
    
#ifndef NO_MPI
	MPI_Barrier(MPI_COMM_WORLD);
#endif
    
    //************** Build the Hilbert space ***************************
    
    // 2 electrons with spin on 2 sites; I can add Hubbard U if I want
    
    // label all possible states in occupation number representation
    vector<double> nph0(NHILB);
    vector<double> nph1(NHILB);
    vector<double> nup0(NHILB);
    vector<double> ndn0(NHILB);
    vector<double> nup1(NHILB);
    vector<double> ndn1(NHILB);
    vector<double> nel0(NHILB);
    vector<double> nel1(NHILB);
    
    // fermionic ordering to clarify signs: site 0 left, site 1 right; up left, down right
    // electrons |up down ; 0 0>
    for(int n0=0; n0<NPHON; ++n0)
        for(int n1=0; n1<NPHON; ++n1)
        {
            nph0[n0 + n1*NPHON] = n0;
            nph1[n0 + n1*NPHON] = n1;
            nup0[n0 + n1*NPHON] = 1;
            ndn0[n0 + n1*NPHON] = 1;
            nup1[n0 + n1*NPHON] = 0;
            ndn1[n0 + n1*NPHON] = 0;
            nel0[n0 + n1*NPHON] = 2;
            nel1[n0 + n1*NPHON] = 0;
        }
    
    // electrons |up 0 ; 0 down>
    for(int n0=0; n0<NPHON; ++n0)
        for(int n1=0; n1<NPHON; ++n1)
        {
            nph0[NHILBPHON + n0 + n1*NPHON] = n0;
            nph1[NHILBPHON + n0 + n1*NPHON] = n1;
            nup0[NHILBPHON + n0 + n1*NPHON] = 1;
            ndn0[NHILBPHON + n0 + n1*NPHON] = 0;
            nup1[NHILBPHON + n0 + n1*NPHON] = 0;
            ndn1[NHILBPHON + n0 + n1*NPHON] = 1;
            nel0[NHILBPHON + n0 + n1*NPHON] = 1;
            nel1[NHILBPHON + n0 + n1*NPHON] = 1;
        }
    
    // electrons |0 down ; up 0>
    for(int n0=0; n0<NPHON; ++n0)
        for(int n1=0; n1<NPHON; ++n1)
        {
            nph0[2*NHILBPHON + n0 + n1*NPHON] = n0;
            nph1[2*NHILBPHON + n0 + n1*NPHON] = n1;
            nup0[2*NHILBPHON + n0 + n1*NPHON] = 0;
            ndn0[2*NHILBPHON + n0 + n1*NPHON] = 1;
            nup1[2*NHILBPHON + n0 + n1*NPHON] = 1;
            ndn1[2*NHILBPHON + n0 + n1*NPHON] = 0;
            nel0[2*NHILBPHON + n0 + n1*NPHON] = 1;
            nel1[2*NHILBPHON + n0 + n1*NPHON] = 1;
        }
    
    // electrons |0 0 ; up down>
    for(int n0=0; n0<NPHON; ++n0)
        for(int n1=0; n1<NPHON; ++n1)
        {
            nph0[3*NHILBPHON + n0 + n1*NPHON] = n0;
            nph1[3*NHILBPHON + n0 + n1*NPHON] = n1;
            nup0[3*NHILBPHON + n0 + n1*NPHON] = 0;
            ndn0[3*NHILBPHON + n0 + n1*NPHON] = 0;
            nup1[3*NHILBPHON + n0 + n1*NPHON] = 1;
            ndn1[3*NHILBPHON + n0 + n1*NPHON] = 1;
            nel0[3*NHILBPHON + n0 + n1*NPHON] = 0;
            nel1[3*NHILBPHON + n0 + n1*NPHON] = 2;
        }
    
    
    // build static Hamiltonian
    cmat Ham0(NHILB, NHILB);
    cmat Hamkin(NHILB, NHILB);
    cmat Hamu(NHILB, NHILB);
    cmat Hamdocc(NHILB, NHILB);
    cmat Hamu0(NHILB, NHILB); // initial U0 for U quench
    cmat Hamph(NHILB, NHILB);
    cmat Hamx(NHILB, NHILB); // X term, to be multiplied by f(t)
    cmat Hamg1(NHILB, NHILB); // linear interaction
    cmat Hamg2(NHILB, NHILB); // nonlinear interaction
    for(int i=0; i<NHILB; ++i)
    {
        // TESTING:
        //Hamkin(i,i) = 1.5 + 0.25*nel0[i] + 0.75*nel1[i];
        
        int ielec = int(i/NHILBPHON); // whether we are in sector 0, 1, 2, 3
        Hamph(i,i) = OMEGA * (nph0[i]+nph1[i]); // automatically diagonal in electrons, since we are in diagonal part
        Hamg2(i,i) = G2 * (nel0[i] * (2.*nph0[i]+1.) + nel1[i] * (2.*nph1[i]+1.)) ; // from 2n+1 term in (b+bdag)^2
        
        if(ielec == 0 || ielec == 3)
        {
            Hamu(i,i) = U1;
            Hamdocc(i,i) = 1.;
            Hamu0(i,i) = U0;
        }
        
        for(int j=0; j<NHILB; ++j)
        {
            int jelec = int(j/NHILBPHON);
            
            if( ((ielec == 1 && jelec == 0) || (ielec == 0 && jelec == 1) || (ielec == 1 && jelec == 3) || (ielec == 3 && jelec == 1)) && nph0[i]==nph0[j] && nph1[i]==nph1[j])
            {
                Hamkin(i,j) = -JHOP;
            }
            
            if( ((ielec == 2 && jelec == 0) || (ielec == 0 && jelec == 2) || (ielec == 2 && jelec == 3) || (ielec == 3 && jelec == 2)) && nph0[i]==nph0[j] && nph1[i]==nph1[j])
            {
                Hamkin(i,j) = JHOP;
            }
            
            if(nph0[i]==nph0[j]+1 && nph1[i]==nph1[j] && ielec == jelec)
            {
                Hamg1(i,j) = G1 * nel0[j] * sqrt(nph0[j]+1);
                Hamx(i,j) = sqrt(nph0[j]+1);
            }
            
            if(nph0[i]==nph0[j]-1 && nph1[i]==nph1[j]  && ielec == jelec)
            {
                Hamg1(i,j) = G1 * nel0[j] * sqrt(nph0[j]);
                Hamx(i,j) = sqrt(nph0[j]); // automatically 0 for nph=0 state
            }
            
            if(nph1[i]==nph1[j]+1 && nph0[i]==nph0[j]  && ielec == jelec)
            {
                Hamg1(i,j) = G1 * nel1[j] * sqrt(nph1[j]+1);
                Hamx(i,j) = sqrt(nph1[j]+1);
            }
            
            if(nph1[i]==nph1[j]-1 && nph0[i]==nph0[j]  && ielec == jelec)
            {
                Hamg1(i,j) = G1 * nel1[j] * sqrt(nph1[j]);
                Hamx(i,j) = sqrt(nph1[j]); // automatically 0 for nph=0 state
            }
            
            if(nph0[i]==nph0[j]+2 && nph1[i]==nph1[j] && ielec == jelec)
                Hamg2(i,j) = G2 * nel0[j] * sqrt((nph0[j]+2)*(nph0[j]+1));
            
            if(nph0[i]==nph0[j]-2 && nph1[i]==nph1[j]  && ielec == jelec)
                Hamg2(i,j) = G2 * nel0[j] * sqrt((nph0[j])*(nph0[j]-1)); // automatically 0 for nph=0 state
            
            if(nph1[i]==nph1[j]+2 && nph0[i]==nph0[j]  && ielec == jelec)
                Hamg2(i,j) = G2 * nel1[j] * sqrt((nph1[j]+2)*(nph1[j]+1));
            
            if(nph1[i]==nph1[j]-2 && nph0[i]==nph0[j]  && ielec == jelec)
                Hamg2(i,j) = G2 * nel1[j] * sqrt((nph1[j])*(nph1[j]-1)); // automatically 0 for nph=0 state
            
            
        }
        
    }
    
    for(int i=0; i<NHILB; ++i)
        for(int j=0; j<NHILB; ++j)
            Ham0(i,j) = Hamph(i,j) + Hamkin(i,j) + Hamu0(i,j) + Hamg1(i,j) + Hamg2(i,j);
    
    /*
    Ham0 += Hamph;
    Ham0 += Hamkin;
    Ham0 += Hamg1;
    Ham0 += Hamg2;
    */
    
    /*
    cout << endl;
    cout << "Print g1 Hamiltonian" << endl;
    for(int i=0; i<NHILB; ++i)
    {
        for(int j=0; j<NHILB; ++j)
        {
            cout << real(Hamg1(i,j)) << '\t';
        }
        cout << endl;
    }
    cout << endl;
    */
    
    
    // REDUCED HILBERT SPACE FOR N-1 PARTICLES (1 electron, spin down, to fix it)
    // ADDED HILBERT SPACE FOR N+1 PARTICLES (2 up spins, 1 down spin remaining)
    
    vector<double> nel0red(NHILBREDUCED);
    vector<double> nel1red(NHILBREDUCED);
    
    vector<double> nel0added(NHILBREDUCED);
    vector<double> nel1added(NHILBREDUCED);
    
    // fermionic ordering to clarify signs: site 0 left, site 1 right; up left, down right
    // electrons |0 down ; 0 0>
    for(int n0=0; n0<NPHON; ++n0)
        for(int n1=0; n1<NPHON; ++n1)
        {
            nel0red[n0 + n1*NPHON] = 1;
            nel1red[n0 + n1*NPHON] = 0;
        }
    
    // electrons |0 0 ; 0 down>
    for(int n0=0; n0<NPHON; ++n0)
        for(int n1=0; n1<NPHON; ++n1)
        {
            nel0red[NHILBPHON + n0 + n1*NPHON] = 0;
            nel1red[NHILBPHON + n0 + n1*NPHON] = 1;
        }
    
    // fermionic ordering to clarify signs: site 0 left, site 1 right; up left, down right
    // electrons |up down ; up 0>
    for(int n0=0; n0<NPHON; ++n0)
        for(int n1=0; n1<NPHON; ++n1)
        {
            nel0added[n0 + n1*NPHON] = 2;
            nel1added[n0 + n1*NPHON] = 1;
        }
    
    // electrons |up 0 ; up down>
    for(int n0=0; n0<NPHON; ++n0)
        for(int n1=0; n1<NPHON; ++n1)
        {
            nel0added[NHILBPHON + n0 + n1*NPHON] = 1;
            nel1added[NHILBPHON + n0 + n1*NPHON] = 2;
        }
    
    cmat Ham0red(NHILBREDUCED, NHILBREDUCED);
    cmat Hamxred(NHILBREDUCED, NHILBREDUCED);
    
    cmat Ham0added(NHILBREDUCED, NHILBREDUCED);
    cmat Hamxadded(NHILBREDUCED, NHILBREDUCED);
    
    for(int i=0; i<NHILBREDUCED; ++i)
    {
        
        int ielec = int(i/NHILBPHON); // whether we are in sector 0, 1
        Ham0red(i,i) += OMEGA * (nph0[i]+nph1[i]); // automatically diagonal in electrons, since we are in diagonal part; phonon occupations are independent of electron sector and also work in reduced space! :-)
        Ham0added(i,i) += OMEGA * (nph0[i]+nph1[i]);
        
        Ham0red(i,i) += G2 * (nel0red[i] * (2.*nph0[i]+1.) + nel1red[i] * (2.*nph1[i]+1.)) ; // from 2n+1 term in (b+bdag)^2
        Ham0added(i,i) += G2 * (nel0added[i] * (2.*nph0[i]+1.) + nel1added[i] * (2.*nph1[i]+1.)) ;
        
        for(int j=0; j<NHILBREDUCED; ++j)
        {
            int jelec = int(j/NHILBPHON); // whether we are in sector 0, 1
            
            if( ((ielec == 1 && jelec == 0) || (ielec == 0 && jelec == 1)) && nph0[i]==nph0[j] && nph1[i]==nph1[j])
            {
                Ham0red(i,j) = -JHOP;
                Ham0added(i,j) = JHOP; // CAREFUL WITH SIGN - HERE WE ARE HOPPING BETWEEN OTHER STATES ...
            }
            
            if(nph0[i]==nph0[j]+1 && nph1[i]==nph1[j] && ielec == jelec)
            {
                Ham0red(i,j) = G1 * nel0red[j] * sqrt(nph0[j]+1);
                Hamxred(i,j) = sqrt(nph0[j]+1);
                Ham0added(i,j) = G1 * nel0added[j] * sqrt(nph0[j]+1);
                Hamxadded(i,j) = sqrt(nph0[j]+1);
            }
            
            if(nph0[i]==nph0[j]-1 && nph1[i]==nph1[j]  && ielec == jelec)
            {
                Ham0red(i,j) = G1 * nel0red[j] * sqrt(nph0[j]);
                Hamxred(i,j) = sqrt(nph0[j]); // automatically 0 for nph=0 state
                Ham0added(i,j) = G1 * nel0added[j] * sqrt(nph0[j]);
                Hamxadded(i,j) = sqrt(nph0[j]); // automatically 0 for nph=0 state
            }
            
            if(nph1[i]==nph1[j]+1 && nph0[i]==nph0[j]  && ielec == jelec)
            {
                Ham0red(i,j) = G1 * nel1red[j] * sqrt(nph1[j]+1);
                Hamxred(i,j) = sqrt(nph1[j]+1);
                Ham0added(i,j) = G1 * nel1added[j] * sqrt(nph1[j]+1);
                Hamxadded(i,j) = sqrt(nph1[j]+1);
            }
            
            if(nph1[i]==nph1[j]-1 && nph0[i]==nph0[j]  && ielec == jelec)
            {
                Ham0red(i,j) = G1 * nel1red[j] * sqrt(nph1[j]);
                Hamxred(i,j) = sqrt(nph1[j]); // automatically 0 for nph=0 state
                Ham0added(i,j) = G1 * nel1added[j] * sqrt(nph1[j]);
                Hamxadded(i,j) = sqrt(nph1[j]); // automatically 0 for nph=0 state
            }
            
            if(nph0[i]==nph0[j]+2 && nph1[i]==nph1[j] && ielec == jelec)
            {
                Ham0red(i,j) = G2 * nel0red[j] * sqrt((nph0[j]+2)*(nph0[j]+1));
                Ham0added(i,j) = G2 * nel0added[j] * sqrt((nph0[j]+2)*(nph0[j]+1));
            }
            
            if(nph0[i]==nph0[j]-2 && nph1[i]==nph1[j]  && ielec == jelec)
            {
                Ham0red(i,j) = G2 * nel0red[j] * sqrt((nph0[j])*(nph0[j]-1)); // automatically 0 for nph=0 state
                Ham0added(i,j) = G2 * nel0added[j] * sqrt((nph0[j])*(nph0[j]-1));
            }
            
            if(nph1[i]==nph1[j]+2 && nph0[i]==nph0[j]  && ielec == jelec)
            {
                Ham0red(i,j) = G2 * nel1red[j] * sqrt((nph1[j]+2)*(nph1[j]+1));
                Ham0added(i,j) = G2 * nel1added[j] * sqrt((nph1[j]+2)*(nph1[j]+1));
            }
            
            if(nph1[i]==nph1[j]-2 && nph0[i]==nph0[j]  && ielec == jelec)
            {
                Ham0red(i,j) = G2 * nel1red[j] * sqrt((nph1[j])*(nph1[j]-1)); // automatically 0 for nph=0 state
                Ham0added(i,j) = G2 * nel1added[j] * sqrt((nph1[j])*(nph1[j]-1));
            }
            
            
        }
        
    }
    
    
    
    cmat Annihilator(NHILBREDUCED, NHILB); // operator that produces N-1 particle wave function from N particle wave function, annihilating up-electron on site 0
    for(int i=0; i<NHILBREDUCED; ++i)
    {
        //int ielec = int(i/NHILBPHON); // whether we are in sector 0, 1
        for(int j=0; j<NHILB; ++j)
        {
            //int jelec = int(j/NHILBPHON); // whether we are in sector 0, 1, 2, 3
            if(i==j)
                Annihilator(i,j) = 1; // really that simple ... note that this is not a diagonal matrix
        }
    }
    
    
    
    // check sign of hopping for added term above
    // sectors are 21, 12
    // sign is "-" ==> +JHOP
    
    
    cmat Creator(NHILBREDUCED, NHILB); // operator that produces N+1 particle wave function from N particle wave function, creating up-electron on site 0
    for(int i=0; i<NHILBREDUCED; ++i)
    {
        //int ielec = int(i/NHILBPHON); // whether we are in sector 0, 1
        for(int j=0; j<NHILB; ++j)
        {
            //int jelec = int(j/NHILBPHON); // whether we are in sector 0, 1, 2, 3
            if(i==j-2*NHILBPHON)
                Creator(i,j) = 1; // really that simple ... note that this is not a diagonal matrix // we are mapping 0 => null, 1 => null, 2 => 0, 3 => 1
        }
    }
    
    // END CONSTRUCTION FOR REDUCED HILBERT SPACE
    
    
    
    // diagonalize Ham0 to find ground state
    cmat Dum(NHILB, NHILB);
    vector<double> evals(NHILB);
    for(int i=0; i<NHILB; ++i)
        for(int j=0; j<NHILB; ++j)
            Dum(i,j) = Ham0(i,j);
    
    diagonalize(Dum, evals, NHILB);
    
    double groundstate_energy = evals[0];
    
    // print ground state energy
    if(myrank==0)
        cout << "lowest energies = " << evals[0] << ", " << evals[1] << endl;
    
    // check if ground state is correct
    vector<cdouble> groundstate(NHILB);
    for(int i=0; i<NHILB; ++i)
    {
        groundstate[i] = Dum(i,0);
    }
    
    vector<cdouble> Hgroundstate(NHILB);
    for(int i=0; i<NHILB; ++i)
        for(int j=0; j<NHILB; ++j)
            Hgroundstate[i] += Ham0(i,j) * groundstate[j];
    
    double energy = 0;
    for(int i=0; i<NHILB; ++i)
        energy += (conj(groundstate[i]) * Hgroundstate[i]).real();
    
    if(myrank==0)
        cout << "ground state energy checked = " << energy << endl;
    
    double dtstep = TMAX/NUMT;
    
    vector<cdouble> psi0(NHILB);
    for(int i=0; i<NHILB; ++i)
        psi0[i] = groundstate[i];
    
    vector<cdouble> psi1(NHILB);
    cmat A1(NHILB,NHILB);
    cmat A2(NHILB,NHILB);
    
    vector<double> tlist(NUMT+1);
    for(int it=0; it<=NUMT; ++it)
        tlist[it] = it*dtstep;
    
    
    for(int i=0; i<NHILB; ++i)
        for(int j=0; j<NHILB; ++j)
            Ham0(i,j) = Hamph(i,j) + Hamkin(i,j) + Hamu(i,j) + Hamg1(i,j) + Hamg2(i,j);
    
    ofstream outfile;
    ofstream outarpes;
    ofstream outarpese;
    
    if(myrank==0)
    {
        outfile.open("out.dat");
        outfile << setw(4) << setprecision(15);
        outfile << "# time" << '\t' << "ekin" << '\t' << "epot" << '\t' << "ekin+epot" << '\t' << "ephon" << '\t' << "etot" << '\t' << "x(t)" << '\t' << "field" << '\t' << "docc" << endl;
    }
    
    if(myrank==0)
    {
        outarpes.open("arpes.dat");
        outarpes << setw(4) << setprecision(15);
        outarpes << "# center time TARPES = " << '\t' << TARPES << endl;
        outarpes << "# omega" << '\t' << "arpes" << '\t' << "retarded" << endl;
    }
    
    if(myrank==0)
    {
        outarpese.open("arpesequil.dat");
        outarpese << setw(4) << setprecision(15);
        outarpese << "# omega" << '\t' << "arpes" << endl;
    }
    
    vector<cdouble> psi0annil(NHILBREDUCED);
    for(int i=0; i<NHILBREDUCED; ++i)
    {
        psi0annil[i] = 0.;
        for(int j=0; j<NHILB; ++j)
            psi0annil[i] += Annihilator(i,j) * psi0[j];
    }
    
    double eta = 1./(2.*SIGARPES); // width for broadening
    double dom = (OMEGAMAX-OMEGAMIN)/(NOMEGAARPES);
    
    for(int iw = 0; iw <= NOMEGAARPES; ++iw)
    {
        cdouble arpeseq = 0.;
        double omegaarpes = OMEGAMIN + dom * iw;
        cmat resolvent(NHILBREDUCED, NHILBREDUCED);
        
        for(int i=0; i<NHILBREDUCED; ++i)
        {
            resolvent(i,i) = omegaarpes + II * eta - groundstate_energy; // important to add E_0 here
            for(int j=0; j<NHILBREDUCED; ++j)
            {
                resolvent(i,j) += Ham0red(i,j);
            }
        }
        
        // invert w + i eta - H
        invert(resolvent.ptr(), NHILBREDUCED);
        
        // take trace with annihilated state
        for(int i=0; i<NHILBREDUCED; ++i)
        {
            cdouble psidum = 0.;
            for(int j=0; j<NHILBREDUCED; ++j)
            {
                psidum += resolvent(i,j)*psi0annil[j];
            }
            arpeseq += conj(psi0annil[i]) * psidum;
        }
        
        arpeseq *= -2./M_PI;
        
        if(myrank==0) outarpese << omegaarpes << '\t' << imag(arpeseq) << endl;
        
    }
    
    
    // FOR ARPES PREPS
    // need to store wavefunction at ARPES time points
    vector<double> arpestlist(2*NTARPES+1);
    vector<int> tmapping(2*NTARPES+1); // maps to actual propagation time points; careful, this requires overlapping grids
    double dtarpes = 6.*SIGARPES/(2*NTARPES);
    for(int it=-NTARPES; it<=NTARPES; ++it)
    {
        arpestlist[it+NTARPES] = double(it)*dtarpes + TARPES; // TARPES is the central time
        tmapping[it+NTARPES] = int(arpestlist[it+NTARPES]/dtstep);
        if(myrank==0) cout << "ARPES step: " << it+NTARPES << " , actual time step: " << tmapping[it+NTARPES] << endl;
    }
    
    // wavefunction storage // only needed in reduced space!!!
    cmat psireduced(2*NTARPES+1, NHILBREDUCED);
    // this is for the addition of a particle in order to compute the addition spectrum
    cmat psiadded(2*NTARPES+1, NHILBREDUCED);
    // END FOR ARPES PREPS
    
    
    // time stepping
    for(int it=1; it<=NUMT; ++it)
    {
        cmat U(NHILB,NHILB);
        
        double time1 = tlist[it-1] + (0.5-sqrt(3.)/6.) * dtstep;
        double time2 = tlist[it-1] + (0.5+sqrt(3.)/6.) * dtstep;
        

        double field1 = FMAX * sin(OMEGAPUMP * time1);
        double field2 = FMAX * sin(OMEGAPUMP * time2);
        double field = FMAX * sin(OMEGAPUMP * tlist[it]);

        
/*
        double field1 = FMAX * sin(OMEGAPUMP * (time1-FCENTER)) * shape(time1);
        double field2 = FMAX * sin(OMEGAPUMP * (time2-FCENTER)) * shape(time1);
        double field = FMAX * sin(OMEGAPUMP * (tlist[it]-FCENTER)) * shape(tlist[it]);
*/
        
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
            {
                A1(i,j) = Ham0(i,j) + field1 * Hamx(i,j);
                A2(i,j) = Ham0(i,j) + field2 * Hamx(i,j);
            }

        
        set_U_step(U, A1, A2, evals, NHILB, dtstep);
        
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                psi1[i] += U(i,j) * psi0[j];
        
        for(int i=0; i<NHILB; ++i)
        {
            psi0[i] = psi1[i];
            psi1[i] = 0.;
        }
        
        
        // store ARPES wavefunction; this is very bad programming since I go thru the whole loop every time
        for(int ita=-NTARPES; ita<=NTARPES; ++ita)
        {
            if(tmapping[ita+NTARPES] == it) // times matching at end of propagation step
            {
                for(int i=0; i<NHILBREDUCED; ++i)
                {
                    psireduced(ita+NTARPES, i) = 0;
                    psiadded(ita+NTARPES, i) = 0;
                    for(int j=0; j<NHILB; ++j)
                    {
                        psireduced(ita+NTARPES, i) += Annihilator(i,j) * psi0[j];
                        psiadded(ita+NTARPES, i) += Creator(i,j) * psi0[j];
                    }
                }
            }
        }
         
        
        vector<cdouble> Hkinpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Hkinpsi0[i] += Hamkin(i,j) * psi0[j];
        
        double ekin = 0;
        for(int i=0; i<NHILB; ++i)
            ekin += (conj(psi0[i]) * Hkinpsi0[i]).real();
        
        vector<cdouble> Hpotpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Hpotpsi0[i] += Hamu(i,j) * psi0[j];
        
        double epot = 0;
        for(int i=0; i<NHILB; ++i)
            epot += (conj(psi0[i]) * Hpotpsi0[i]).real();
        
        //double occ.
        vector<cdouble> Hdoccpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Hdoccpsi0[i] += Hamdocc(i,j) * psi0[j];
        
        double docc = 0;
        for(int i=0; i<NHILB; ++i)
            docc += (conj(psi0[i]) * Hdoccpsi0[i]).real();
        
        vector<cdouble> Hphonpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Hphonpsi0[i] += Hamph(i,j) * psi0[j];
        
        double ephon = 0;
        for(int i=0; i<NHILB; ++i)
            ephon += (conj(psi0[i]) * Hphonpsi0[i]).real();
        
        vector<cdouble> Htotpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Htotpsi0[i] += Ham0(i,j) * psi0[j];
        
        double etot = 0;
        for(int i=0; i<NHILB; ++i)
            etot += (conj(psi0[i]) * Htotpsi0[i]).real();
        
        vector<cdouble> Hxpsi0(NHILB);
        for(int i=0; i<NHILB; ++i)
            for(int j=0; j<NHILB; ++j)
                Hxpsi0[i] += Hamx(i,j) * psi0[j];
        
        double xt = 0;
        for(int i=0; i<NHILB; ++i)
            xt += (conj(psi0[i]) * Hxpsi0[i]).real();
        
        if(myrank==0)
        {
            cout << "time = " << tlist[it];
            cout << ", kinetic energy = " << ekin;
            cout << ", Hubbard energy = " << epot;
            cout << ", electronic energy = " << ekin+epot;
            cout << ", phononic energy = " << ephon;
            cout << ", x(t) = " << xt;
            cout << ", total energy = " << etot;
            cout << ", double occ = " << docc << endl;
            
            outfile << tlist[it] << '\t' << ekin << '\t' << epot << '\t' << ekin+epot << '\t' << ephon << '\t' << etot << '\t' << xt << '\t' << field << '\t' << docc << endl;
        }
        
    }
    
    
    // construction for spectrum: need to store wave functions at some predetermined number of time steps grid (reduced sampling?!) - maybe for 50 times or so, depending on probe width
    // then apply Annihilator and propagate using reduced Hilbert space Hamiltonian, accumulate directly to spectrum
    // fix the center of the probe window!!!
    // propagate only for half the times
    
    // now propagate the reduced wavefunction in reduced Hilbert space
    
    
    vector<cdouble> psi0reduced(NHILBREDUCED);
    vector<cdouble> psi1reduced(NHILBREDUCED);
    cmat A1reduced(NHILBREDUCED,NHILBREDUCED);
    cmat A2reduced(NHILBREDUCED,NHILBREDUCED);
    vector<double> evalsreduced(NHILBREDUCED);
    
    vector<cdouble> psi0added(NHILBREDUCED);
    vector<cdouble> psi1added(NHILBREDUCED);
    cmat A1added(NHILBREDUCED,NHILBREDUCED);
    cmat A2added(NHILBREDUCED,NHILBREDUCED);
    vector<double> evalsadded(NHILBREDUCED);
    
    double shaper = 0.;
    vector<double> arpessignal(NOMEGAARPES+1);
    vector<double> retardedsignal(NOMEGAARPES+1);
    double domega = (OMEGAMAX-OMEGAMIN)/(NOMEGAARPES);
    
    if(myrank==0) cout << "Starting ARPES postprocessing" << endl;
    
    for(int it=-NTARPES+myrank; it<=NTARPES; it+=numprocs)
    {
        // start propagation from respective reference state at (t,t)
        // only propagate forward in time
        
        if(myrank==0) cout << "ARPES at time step " << it+NTARPES << " out of " << 2*NTARPES+1 << endl;
        
        for(int i=0; i<NHILBREDUCED; ++i)
        {
            psi0reduced[i] = psireduced(it+NTARPES, i);
            psi0added[i] = psiadded(it+NTARPES, i);
        }
        
        cmat Ureduced(NHILBREDUCED, NHILBREDUCED);
        cmat Uadded(NHILBREDUCED, NHILBREDUCED);

        for(double time=arpestlist[it+NTARPES]; time<arpestlist[2*NTARPES]; time += dtstep)
        {
            double time1 = time + (0.5-sqrt(3.)/6.) * dtstep;
            double time2 = time + (0.5+sqrt(3.)/6.) * dtstep;
            double field1 = FMAX * sin(OMEGAPUMP * time1);
            double field2 = FMAX * sin(OMEGAPUMP * time2);
            
            for(int i=0; i<NHILBREDUCED; ++i)
                for(int j=0; j<NHILBREDUCED; ++j)
                {
                    A1reduced(i,j) = Ham0red(i,j) + field1 * Hamxred(i,j);
                    A2reduced(i,j) = Ham0red(i,j) + field2 * Hamxred(i,j);
                    
                    A1added(i,j) = Ham0added(i,j) + field1 * Hamxadded(i,j);
                    A2added(i,j) = Ham0added(i,j) + field2 * Hamxadded(i,j);
                }
            
            
            set_U_step(Ureduced, A1reduced, A2reduced, evalsreduced, NHILBREDUCED, dtstep);
            set_U_step(Uadded, A1added, A2added, evalsadded, NHILBREDUCED, dtstep);
            
          
            
            for(int i=0; i<NHILBREDUCED; ++i)
                for(int j=0; j<NHILBREDUCED; ++j)
                {
                    psi1reduced[i] += Ureduced(i,j) * psi0reduced[j];
                    psi1added[i] += Uadded(i,j) * psi0added[j];
                }
            
            for(int i=0; i<NHILBREDUCED; ++i)
            {
                psi0reduced[i] = psi1reduced[i];
                psi1reduced[i] = 0.;
                
                psi0added[i] = psi1added[i];
                psi1added[i] = 0.;
            }
            
            
            
            
            // accumulate to ARPES signal at t'-t when congruent with arpestlist
            // use symmetry for other half of the t'-t triangle
            for(int ittest=-NTARPES; ittest<=NTARPES; ++ittest)
            {
                if(arpestlist[ittest+NTARPES] == time+dtstep) // the time to which we have just propagated
                {
                    // compute overlap
                    cdouble overlap = 0.;
                    cdouble overlapadded = 0.;
                    for(int i=0; i<NHILBREDUCED; ++i)
                    {
                        overlap += conj(psireduced(ittest+NTARPES, i)) * psi0reduced[i];
                        overlapadded += conj(psiadded(ittest+NTARPES, i)) * psi0added[i];
                    }
                    
                    // add to ARPES
                    
                    for(int iw = 0; iw <= NOMEGAARPES; ++iw)
                    {
                        double omegaarpes = OMEGAMIN + domega * iw;
                        shaper = arpesshape(arpestlist[ittest+NTARPES], arpestlist[it+NTARPES], TARPES, SIGARPES);
                        
                        
                        arpessignal[iw] += 2.*real(exp(II * omegaarpes * (arpestlist[it+NTARPES] - arpestlist[ittest+NTARPES])) * overlap) *  dtarpes * dtarpes * shaper;
                        
                        retardedsignal[iw] += 2.*real(exp(II * omegaarpes * (arpestlist[ittest+NTARPES] - arpestlist[it+NTARPES])) * overlapadded) *  dtarpes * dtarpes * shaper;
                        
                        retardedsignal[iw] += 2.*real(exp(II * omegaarpes * (arpestlist[it+NTARPES] - arpestlist[ittest+NTARPES])) * overlap) *  dtarpes * dtarpes * shaper;
                        
                    }
                }
            }
            
            
            
        }
            
        cdouble overlapdiag = 0.; // not necessarily = 1 in general because it contains info about particle number
        
        cdouble overlapdiagadded = 0.;
        
        for(int i=0; i<NHILBREDUCED; ++i)
        {
            overlapdiag += conj(psireduced(it+NTARPES, i)) * psireduced(it+NTARPES, i);
            overlapdiagadded += conj(psiadded(it+NTARPES, i)) * psiadded(it+NTARPES, i);
        }
        
        // add diagonal t-t term at the end
        shaper = arpesshape(arpestlist[it+NTARPES], arpestlist[it+NTARPES], TARPES, SIGARPES);
        for(int iw = 0; iw <= NOMEGAARPES; ++iw)
        {
            arpessignal[iw] += real(overlapdiag) * dtarpes * dtarpes * shaper ;
            retardedsignal[iw] += real(overlapdiag) * dtarpes * dtarpes * shaper ;
            retardedsignal[iw] += real(overlapdiagadded) * dtarpes * dtarpes * shaper ;
        }
        
    }
    
#ifndef NO_MPI
    MPI_Allreduce(MPI_IN_PLACE, &arpessignal[0], NOMEGAARPES+1,
                  MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE, &retardedsignal[0], NOMEGAARPES+1,
                  MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif
    
    for(int iw = 0; iw <= NOMEGAARPES; ++iw)
        if(myrank==0) outarpes << OMEGAMIN + domega * iw << '\t' << arpessignal[iw] << '\t' << retardedsignal[iw] << endl;
    
    
    
    if(myrank==0)
        cout << "\n ... done! \n"<<flush;
    
    
#ifndef NO_MPI
	MPI_Finalize();
#endif
    
    return 0;

}

